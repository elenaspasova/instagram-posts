import { ElenaLessonsPage } from './app.po';

describe('elena-lessons App', () => {
  let page: ElenaLessonsPage;

  beforeEach(() => {
    page = new ElenaLessonsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
