import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {JsonpService} from "../jsonp.service";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  access_token = null;
  hashtag1;
  missingHashTag = null;
  res;

  constructor(private activatedRoute: ActivatedRoute,
              private _jsonP: JsonpService) {
    this.activatedRoute.fragment.subscribe((fragment: string) => {
      this.access_token = fragment.split("=")[1];
      console.log(this.access_token);

    });
  }

  returnImages() {
    this.missingHashTag = this.hashtag1;
    this._jsonP.search([
      {access_token: this.access_token},
      {count: 50}

    ], this.hashtag1).subscribe(
      response => {
        this.res = response.data;
        console.log('res', response, this.res);
      }
    );
  }

  ngOnInit() {

  }


}
