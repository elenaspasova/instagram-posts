import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AboutRoutingModule} from './about-routing.module';
import {AboutComponent} from './about.component';
import {JsonpService} from "../jsonp.service";
import { JsonpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    AboutRoutingModule,
    CommonModule,
    JsonpModule,
    FormsModule
  ],
  declarations: [AboutComponent],
  providers: [JsonpService]
})
export class AboutModule { }
