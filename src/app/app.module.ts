import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import {AppRoutingModule} from './app.routing';
import { HomeComponent } from './home/home.component';
import {ComService} from "./com-service.service";
import {JsonpService} from "./jsonp.service";

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule

  ],
  providers: [ComService,JsonpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
