import {Injectable} from "@angular/core";
import './rxjs-operators';
import {Headers, Http, Response} from "@angular/http";

@Injectable()
export class ComService {
  API = 'http://localhost:3000';

  constructor(public _http: Http) {
  }

  post(URI, Object, skipAuth?: boolean) {
    let headers = new Headers();

    if (!skipAuth) {
      headers.append('Authorization', localStorage.getItem('token'));
    }

    return this._http.post('localhost:3000' + URI,
      {
        Object
      }, {headers: headers})
    // ...and calling .json() on the response to return data
      .map((res: Response) => res.json());
    //...errors if any


  }


}
