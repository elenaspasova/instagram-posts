import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  clientId = '9ac4b3e7cf4445a98231b548770188ce';
  //Instagram Api has not approved the application
  // so we can only test with hardcoded client ids

  constructor() {
  }

  ngOnInit() {
    window.location.href = 'https://api.instagram.com/oauth/authorize/' +
      '?client_id=' + this.clientId + '&redirect_uri=http://localhost:4200/about/&response_type=token&scope=public_content';

  }

}
