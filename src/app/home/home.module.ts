import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from "./home.component";
import {FormsModule} from "@angular/forms";
import {JsonpService} from "../jsonp.service";

@NgModule({
  imports: [
    HomeRoutingModule,
    CommonModule,
    FormsModule
  ],
  declarations: [HomeComponent],
  providers: [JsonpService]
})
export class HomeModule { }
