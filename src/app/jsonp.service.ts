import {Injectable} from '@angular/core';
import {Jsonp, URLSearchParams} from "@angular/http";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class JsonpService {
  // url = 'https://idx.thinktankconnect.com//v1/Properties.jsonp?apiKey=e4d1c51c064a55856256d1366923f1ef13562cc6&function=getdata&membership_id=20&category=all&lat=0&lng=0&radius=10&get_count=10&neighborhood_id=&condonames=&status=available&location=St%20Petersburg&offset=0&limit=15&sort=list_price&sort_dir=desc&';
  url = 'https://api.instagram.com/v1/tags/';
  times = 10;

  constructor(private jsonp: Jsonp) {
    this.times = 0;
  }


  // search (term: string) {
  //   let wikiUrl = 'http://en.wikipedia.org/w/api.php';
  //   let params = new URLSearchParams();
  //   params.set('search', term); // the user's search value
  //   params.set('action', 'opensearch');
  //   params.set('format', 'json');
  //   params.set('callback', `__ng_jsonp__.__req${this.times}.finished`);
  //   this.times=this.times+1;
  //    TODO: Add error handling
  //   return this.jsonp
  //     .get(wikiUrl, { search: params })
  //     .map(response => <string[]> response.json()[1]);
  // }


  search(term: any,hashtag:any) {
    const wikiUrl = this.url + hashtag +'/media/recent/';
    const params = new URLSearchParams(); // kazva, 4e param e class
    term.forEach(function (element, index, array) {
      const key = Object.keys(element);
      console.log(key);
      params.set(key[0], element[key[0]]); // key = 'promenliva'; Object[key] == Object.promenliva, t.e. Object[key] = valueto na key;
    });
    params.set('callback', `__ng_jsonp__.__req0.finished`);
    this.times = this.times + 1;
    // TODO: Add error handling
    return this.jsonp
      .get(wikiUrl, {search: params})
      .map(response => response.json());
  }

}
